@if (count($tagsDiretas) > 0)
    <h4>Tags</h4>
    <ul class="nav nav-pills nav-stacked postTags">
        @foreach ($tagsDiretas as $tag)
        <li role="presentation" @if (strtolower($tag->no_termo) == strtolower(Request::get('query'))) class="active" @endif>
            <a href="/?query={{ $tag->no_termo }}">{{ $tag->no_termo }}</a>
        </li>
        @endforeach
    </ul>
@endif

@if (count($tagsRelacionadas) > 0)
    <h4>Tags Relacionadas</h4>
    <ul class="nav nav-pills nav-stacked postTagsRelacionadas">
        @foreach ($tagsRelacionadas as $tag)
        <li role="presentation" @if (strtolower($tag->no_termo) == strtolower(Request::get('query'))) class="active" @endif>
            <a href="/?query={{ $tag->no_termo }}">{{ $tag->no_termo }}</a>
        </li>
        @endforeach
    </ul>
@endif