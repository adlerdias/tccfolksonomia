@extends('app')
@section('content')
<div class="container">
	    <div class="row">
	        <div class="col-md-9" role="main">
	            <div class="error-template">
	                <h1>
	                    Oops!</h1>
	                <h2>
	                    404 Não encontrada</h2>
	                <div class="error-details">
	                    {!! $mensagem !!}
	                </div>
	                <div class="error-actions">
	                    <a href="/" class="btn btn-primary btn-lg">
	                    	<span class="glyphicon glyphicon-home"></span>
	                        Voltar para página inicial
	                    </a>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-3" role="complementary">
	            
	        </div>
	    </div>
	</div>
@endsection