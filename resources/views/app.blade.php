<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TccFolksonomia</title>

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bower/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/bower/select2/select2.css">
    <link rel="stylesheet" href="/bower/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="/bower/jquery-joyride/joyride-2.1.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://bra.ifsp.edu.br">IFSP</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a class="gerenciar" href="{{ url('post') }}">Gerenciar Posts</a></li>
                    <!--<li><a href="{{ url('post/dados') }}">Dados Gerais</a></li>-->
                    <li><a id="guia" href="#">Guia de Utilização</a></li>
                </ul>
                <?php
                if (isset($query)):
                ?>
                <div class="col-sm-3 col-md-3">
                    <div class="form-group">
                        {!! Form::open(array('method' => 'GET', 'url' => '/', 'class'=>'form-inline form-inline navbar-form pull-left col-12 col-sm-8 col-lg-6')) !!}
                            <div class="input-group input-group-sm input-group-md col-sm-12 col-md-12">
                                {!! Form::text('query', $query, ['class' => 'form-control', 'id'=>'query', 'placeholder'=>'Pesquisar']) !!}
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <?php
                endif;
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://www.governoeletronico.gov.br/acoes-e-projetos/e-ping-padroes-de-interoperabilidade/vcge">Governo Eletrônico</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')



    <!-- Scripts -->
    <script src="/bower/jquery/dist/jquery.min.js"></script>    
    <script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower/speakingurl/lib/speakingurl.js"></script>
    <script src="/bower/jquery-slugify/dist/slugify.min.js"></script>
    <script src="/bower/select2/select2.min.js"></script>
    <script src="/bower/jquery-joyride/jquery.cookie.js"></script>
    <script src="/bower/jquery-joyride/modernizr.mq.js"></script>
    <script src="/bower/jquery-joyride/jquery.joyride-2.1.js"></script>

    @yield('footer')
    <script>
        $(window).load(function() {
            $( "#guia" ).on( "click", function(evt) {
                evt.preventDefault();
                $('#joyRideTipContent').joyride({
                    autoStart : true,
                    modal:true,
                    expose: true
                });
            });
        });
    </script>
</body>
</html>
