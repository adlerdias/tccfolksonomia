@extends('app')
@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            {!! Form::model($post, ['method'=>'PATCH', 'route' => ['post.update', ['id' => $post->id]], 'class'=>'form-horizontal']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editar Post
                    </div>
                    <div class="panel-body">
                        @include('post._form')
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit('Salvar', ['class' => 'btn btn-sm btn-success btn-addon col-sm-offset-2 btnSalvar']) !!}
                        <a href="{{ route('post') }}" class="btn btn-default btn-sm btn-addon"><i class="glyphicon glyphicon-remove"></i>Cancelar</a>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
