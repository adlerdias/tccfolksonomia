<?php
namespace App;

class Sugestao {

    // Convert an UTF-8 encoded string to a single-byte string suitable for
    // functions such as levenshtein.
    // 
    // The function simply uses (and updates) a tailored dynamic encoding
    // (in/out map parameter) where non-ascii characters are remapped to
    // the range [128-255] in order of appearance.
    //
    // Thus it supports up to 128 different multibyte code points max over
    // the whole set of strings sharing this encoding.
    //
    private function utf8_to_extended_ascii($str, &$map)
    {
        // find all multibyte characters (cf. utf-8 encoding specs)
        $matches = array();
        if (!preg_match_all('/[\xC0-\xF7][\x80-\xBF]+/', $str, $matches))
            return $str; // plain ascii string
        
        // update the encoding map with the characters not already met
        foreach ($matches[0] as $mbc)
            if (!isset($map[$mbc]))
                $map[$mbc] = chr(128 + count($map));
        
        // finally remap non-ascii characters
        return strtr($str, $map);
    }

    // Didactic example showing the usage of the previous conversion function but,
    // for better performance, in a real application with a single input string
    // matched against many strings from a database, you will probably want to
    // pre-encode the input only once.
    //
    private function levenshtein_utf8($s1, $s2) {
        $charMap = array();
        $s1 = $this->utf8_to_extended_ascii($s1, $charMap);
        $s2 = $this->utf8_to_extended_ascii($s2, $charMap);
        
        return levenshtein($s1, $s2);
    }

    public function sugerir($query) {
        $didYouMean = null;
        $closest = null;
        $tags  = \App\Tag::getTodas();
        if ($tags && $query) {
            $shortest = -1;
            foreach ($tags as $tag) {
                $querySize = strlen($query);
                $arrTag = explode(" ", $tag);
                $tagCroppedFromEnd = strtolower($arrTag[count($arrTag)-1]);
                $levFromEnd = $this->levenshtein_utf8(strtolower($query), strtolower($tagCroppedFromEnd));
                $tagCropped = mb_substr(strtolower($tag), 0, $querySize);
                $lev = $this->levenshtein_utf8(strtolower($query), strtolower($tagCropped));
                //echo $tagCropped . " - " . $querySize .  " - " .  $lev . "<br />";
                if ($lev == 0) {
                    $closest = $tag;
                    $shortest = 0;
                    break;
                }
                if ($lev <= $shortest || $shortest < 0) {
                    $closest  = $tag;
                    $shortest = $lev;
                }
                if ($levFromEnd < $lev || $shortest < 0) {
                    $lev = $levFromEnd;
                }
            }
            if ($closest) {
                $didYouMean = "Você quis dizer: <a href=\"/?query=$closest\">$closest</a>?\n";
            } else {
                $didYouMean = "Não foi possível sugerir uma Tag clique <a href=\"/tag/listar\">aqui</a> para conhecer as tags.\n";
            }
        }
        return $didYouMean;
    }

}