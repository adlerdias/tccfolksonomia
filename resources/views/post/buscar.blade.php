@extends('app')
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-9" role="main">
                <div class="row">
                    Utilizando o Termo (tag) "{{ $tagModel->no_termo }}" foram encontrados <b>{!! $posts->total() !!}</b> resultados.
                </div>
                <div class="panel">
                    @foreach($posts as $post)
                        <div class="panel-heading">
                            <div class="text-center">
                                <div class="row">
                                    <h3 class="pull-left">{{ $post->titulo }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            {{ $post->post }} <a href="/exibir/{{ $post->slug }}">Saiba mais</a>
                        </div>
                        <div class="panel-footer">
                            <small>Data do Post <em>{{ $post->created_at }}</em></small><br/>
                            {!! $post->getTagsUrl() !!}
                        </div>
                    @endforeach
                </div>
                {!! $posts->appends(Request::only('pesquisar'))->render() !!}
                <?php //echo $posts->linksibindo 10 (); ?>
            </div>
            <div class="col-md-3" role="complementary">
                <h2>Pesquisar</h2>
                <form class="form-inline">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Buscar" id="pesquisar" name="pesquisar" value="" />
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection