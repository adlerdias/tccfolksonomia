<?php
 
namespace App\Http\Controllers;
 
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;
use App\Post;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pesquisar = null;
        $query = ($request->get("query")) ? $request->get("query") : null;
        $tagModel = \App\Tag::where('no_termo', $query)->first();
        if ($query && $tagModel) {
            $pesquisar = $query;
            $tagsDiretas = $tagModel->tagsDiretas();
            $tagsRelacionadas = $tagModel->tagsRelacionadas();
            $posts = $tagModel->posts()->paginate(10);

            return view(
                'site.listar',
                compact(
                    'query',
                    'tagsDiretas',
                    'tagsRelacionadas',
                    'tagModel',
                    'posts',
                    'pesquisar'
                )
            );
        } else {
            $didYouMean = null;
            if ($query) {
                $sugestao = new \App\Sugestao();
                $didYouMean = $sugestao->sugerir($query);    
                $pesquisar = $query;
            }
            return view(
                'site.index',
                compact(
                    'didYouMean',
                    'pesquisar'
                )
            );
        }
    }

    /**
     * Display the resource.
     *
     * @return Response
     */
    public function exibir($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('post.exibir', compact('post'));
    }
}
 