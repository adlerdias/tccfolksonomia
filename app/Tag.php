<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {
    
    protected $table = 'VCGE_termo';
    protected $primaryKey = 'co_termo';

    /**
     * Get the posts associated to the given Tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts() {
        return $this->belongsToMany('App\Post');
    }

    /*
     *
     * Os Termos Diretos são termos que possuem o termo “trabalho” como termo pai. 
     * Por Exemplo: Fiscalização do Trabalho, Legislação Trabalhista, Mercado de Trabalho, etc...  
     *
     */
    public function tagsDiretas() {
        return app('db')->select("
            SELECT * FROM VCGE_termo WHERE VCGE_termo.co_termo IN (
                SELECT co_termo_A FROM VCGE_relacao WHERE co_termo_B = (
                  SELECT co_termo FROM VCGE_termo
                    WHERE LOWER(no_termo) = '".strtolower($this->no_termo)."')
            ) OR VCGE_termo.co_termo IN (
                SELECT co_termo FROM VCGE_termo
                    WHERE LOWER(no_termo) = '".strtolower($this->no_termo)."'
            ) ORDER BY no_termo
        ");
    }

    /*
     *
     * Os Termos Relacionados são termos onde a palavra “trabalho”  faz parte da sua composição. 
     * Por Exemplo: Trabalho Social, Carteira do Trabalho, Gestão do Trabalho, etc...
     *
     */
    public function tagsRelacionadas() {
        return app('db')->select("
            SELECT * FROM VCGE_termo WHERE LOWER(VCGE_termo.no_termo) LIKE  '%".strtolower($this->no_termo)."%'
        ");
    }

    public static function getTagsByQuery($query) {
        return app('db')->select("SELECT co_termo, no_termo FROM VCGE_termo WHERE LOWER(VCGE_termo.no_termo) LIKE '%".strtolower($query)."%' ORDER BY no_termo");
    }

    public static function getTodas() {
        $tags = app('db')->select('SELECT no_termo FROM VCGE_termo ORDER BY no_termo');
        $arrTags = [];
        foreach ($tags as $tag) {
            $arrTags[] .= $tag->no_termo;
        }
        return $arrTags;
    }
}
