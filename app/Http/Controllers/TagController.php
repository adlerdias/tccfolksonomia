<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Tag;
use Illuminate\Pagination;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TagController extends Controller
{
    /**
     * Display the resource using tag.
     *
     * @return Response
     */
    public function tag($tag, $search = null)
    {
        $post = new \App\Post;
        $posts = $post->buscar($tag, $search);
        $tagModel = \App\Tag::find($tag);

        if (count($posts)>0) {
            return view('post.buscar', compact('posts', 'tagModel', 'search'));
        }

        abort(404, "Desculpe-nos o transtorno, porém, nenhum post foi encontrado com a tag <strong>{$tagModel->no_termo}</strong>");
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function listar(Request $request)
    {
        $tags = \App\Tag::orderBy('no_termo')->get();
        
        return view(
            'tag.index',
            compact(
                'tags'
            )
        );
    }
}
 
 