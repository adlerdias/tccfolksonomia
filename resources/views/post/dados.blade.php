@extends('app')
@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            <strong>Observação:</strong> Cada post pode possuir mais de uma Tag.
            <div class="panel panel-default">
                <table class="table table-bordered has-action">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Quantidade</th>
                            <th>Tags</th>
                            <th>Tags Relacionadas</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dados as $informacao)
                            <tr>
                                <td><a href="/?query={{ $informacao->tag->no_termo }}">{{ $informacao->tag->no_termo }}</a></td>
                                <td>{{ $informacao->quantidade }}</td>
                                <td>
                                    <ul class="nav nav-pills nav-stacked">
                                        @foreach ($informacao->diretas as $tag)
                                        <li role="presentation" @if (strtolower($tag->no_termo) == strtolower(Request::get('query'))) class="active" @endif>
                                            <a href="/?query={{ $tag->no_termo }}">{{ $tag->no_termo }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <ul class="nav nav-pills nav-stacked">
                                        @foreach ($informacao->relacionadas as $tag)
                                        <li role="presentation" @if (strtolower($tag->no_termo) == strtolower(Request::get('query'))) class="active" @endif>
                                            <a href="/?query={{ $tag->no_termo }}">{{ $tag->no_termo }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection