@extends('app')
@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Criar Post
                </div>
                    {!! Form::open(['method'=>'PUT', 'route' => ['post.store'], 'class'=>'form-horizontal']) !!}
                    <div class="panel-body">
                        @include('post._form')
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit('Criar', ['class' => 'btn btn-sm btn-success btn-addon col-sm-offset-2 btnSalvar']) !!}
                        <a href="{{ route('post') }}" class="btn btn-default btn-sm btn-addon"><i class="glyphicon glyphicon-remove"></i>Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection