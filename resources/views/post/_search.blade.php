<div class="form-group">
    {!! Form::open(array('method' => 'GET', 'url' => '/', 'class'=>'form-inline ')) !!}
        <div class="input-group input-group-sm col-sm-12 col-md-12 numero2">
            {!! Form::text('query', $pesquisar, ['class' => 'form-control', 'id'=>'query', 'placeholder'=>'Pesquisar através das Tags ']) !!}
            <div class="input-group-btn">
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    {!! Form::close() !!}
</div>