@extends('app')
@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('post.create') }}" class="btn btn-sm btn-success btn-addon btnAdicionar"><i class="glyphicon glyphicon-plus"></i>Criar</a>
                </div>
                <table class="table table-bordered has-action">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Post</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->titulo }}</td>
                                <td>{{ str_limit($post->post, 150, '...') }}</td>
                                <td>
                                    <a href="{{ route('post.show', ['slug' => $post->slug]) }}" class="btn btn-info btn-sm btnExibir">Exibir</a>
                                    <a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-info btn-sm btnEditar">Editar</a>
                                    <a href="{{ route('post.destroy', ['id' => $post->id]) }}" class="btn btn-danger btn-sm btnRemover">Remover</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row text-center">
            {!! $posts->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('footer')
        <!-- At the bottom of your page but inside of the body tag -->
    <ol id="joyRideTipContent" data-joyride>
        <li data-class="btnAdicionar" data-text="Próximo" data-options="tip_location: top; prev_button: false">
            <p>Utilize o botão criar, para adicionar um novo post.</p>
        </li>
        <li data-class="btnExibir" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Anterior">
            <p>Utilize o botão Exibir, para visualiar o post em questão.</p>
        </li>
        <li data-class="btnEditar" data-button="Próximo" data-prev-text="Anterior" data-options="tip_location:top;tip_animation:fade">
            <p>Utilize o botão Editar, para editar o post em questão.</p>
        </li>
        <li data-class="btnRemover" data-button="Próximo" data-prev-text="Anterior" data-options="tip_animation:fade">
            <p>Utilize o botão Remover, para editar o post em questão.</p>
        </li>
        <li data-button="Ok" data-prev-text="Anterior">
            <p>Agora você já pode utilizar o gerenciamento de posts.</p>
        </li>
    </ol>
@endsection