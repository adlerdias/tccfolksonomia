<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $table = 'posts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'titulo',
        'slug',
        'post',
        'created_at',
        'updated_at',
    ];

    /**
     * Get the tags associated to the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
	public function tags() {
		return $this->belongsToMany('App\Tag');
	}

	/**
     * Get a list of tags associated to the post.
     *
     * @return array
     */
    public function getTagListAttribute() {
        $lista = [];
        foreach ($this->tags->lists('co_termo') as $tag) {
            $lista[] = $tag;
        }
        return $lista;
    }

    /**
     * Get a list of tags associated to the post.
     *
     * @return array
     */
    public function getTagsUrl() {
        $urls = '<ul class="nav nav-pills">';
        foreach ($this->tags as $tag) {
            $urls .= '<li role="presentation"><a class="label label-primary" href="/?query='.$tag->no_termo.'">'.$tag->no_termo.'</a></li>';
        }
        $urls .= '</ul>';
        return $urls;
    }

    public function buscar($tag, $search) {
        return Post::where(function($q) use ($tag, $search)
            {
                !$tag ?: $q->where('post_tag.tag_id', '=', $tag)
                ->havingRaw('count(DISTINCT posts_tags.tag_id) = '. count($tag));

                !$search ?: $q->where('post', 'LIKE','%'.$search.'%');
                !$search ?: $q->where('titulo', 'LIKE','%'.$search.'%');

            })
            ->join('post_tag','post_tag.post_id', '=', 'posts.id')
            ->groupBy('posts.id')
            ->paginate(10);
    }

}