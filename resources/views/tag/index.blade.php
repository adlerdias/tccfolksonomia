@extends('app')
@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <table class="table table-bordered has-action">
                    <thead>
                        <tr>
                            <th id="tagNome">Nome</th>
                            <th id="tagNota">Nota</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $tag)
                            <tr>
                                <td><a href="/?query={{ $tag->no_termo }}">{{ $tag->no_termo }}</a></td>
                                <td>{{ $tag->tx_nota }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
        <!-- At the bottom of your page but inside of the body tag -->
    <ol id="joyRideTipContent" data-joyride>
        <li data-id="tagNome" data-text="Próximo" data-options="tip_location: top; prev_button: false">
            <p>Nome da Tag.</p>
        </li>
        <li data-id="tagNota" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Anterior">
            <p>Nota, opcional, da Tag.</p>
        </li>
        <li data-button="Ok" data-prev-text="Anterior">
            <p>Clicando no nome da Tag é possível efetuar a busca pela mesma.</p>
        </li>
    </ol>
@endsection