<div class="form-group @if ($errors->has('titulo')) has-error @endif">
    <label class="col-sm-2 control-label">Título</label>
    <div class="col-sm-10">
        {!! Form::text('titulo', null, ['class'=>'form-control', 'id'=>'titulo']) !!}
        @if ($errors->has('titulo')) <p class="help-block">{{ $errors->first('titulo') }}</p> @endif
    </div>
</div>

<div class="form-group @if ($errors->has('slug')) has-error @endif">
    <label class="col-sm-2 control-label">Slug</label>
    <div class="col-sm-10">
        {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug']) !!}
        @if ($errors->has('slug')) <p class="help-block">{{ $errors->first('slug') }}</p> @endif
    </div>
</div>

<div class="form-group @if ($errors->has('post')) has-error @endif">
    <label class="col-sm-2 control-label">Post</label>
    <div class="col-sm-10">
        {!! Form::textArea('post', null, ['class'=>'form-control', 'id'=>'post']) !!}
        @if ($errors->has('post')) <p class="help-block">{{ $errors->first('post') }}</p> @endif
    </div>
</div>

<div class="form-group @if ($errors->has('tag_list')) has-error @endif">
    <label class="col-sm-2 control-label">Tags</label>
    <div class="col-sm-10">
        {!! Form::select('tag_list[]', $tags, null, ['class'=>'form-control', 'id'=>'tag_list', 'multiple']) !!}
        @if ($errors->has('tag_list')) <p class="help-block">{{ $errors->first('tag_list') }}</p> @endif
    </div>
</div>
@section('footer')
    <!-- At the bottom of your page but inside of the body tag -->
    <ol id="joyRideTipContent" data-joyride>
        <li data-id="titulo" data-text="Próximo" data-options="tip_location: top; prev_button: false">
            <p>Por favor, preencha o título.</p>
        </li>
        <li data-id="slug" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Prev">
            <p>Durante o preenchimento do título esse campo será preenchido automaticamente.</p>
        </li>
        <li data-id="post" data-button="Próximo" data-prev-text="Anterior" data-options="tip_location:top;tip_animation:fade">
            <p>Por favor, preencha o post.</p>
        </li>
        <li data-class="select2-choices" data-button="Próximo" data-prev-text="Anterior" data-options="tip_animation:fade">
            <p>Por favor, selecione uma ou mais tags.</p>
        </li>
        <li data-class="panel-footer" data-button="Próximo" data-prev-text="Anterior" data-options="tip_animation:fade">
            <p>Em seguida você poderá criar, salvar ou cancelar a criação/edição do seu post.</p>
        </li>
        <li data-button="Ok" data-prev-text="Anterior">
            <p>Obrigado por sua atenção, </p>
        </li>
    </ol>
    <script type="text/javascript">
        $('#tag_list').select2();
        $(document).ready( function() {
            $('form').on('keyup keydown blur paste', '#titulo', function() {
                $('#slug').slugify('#titulo');
            });
        });
    </script>
@endsection
