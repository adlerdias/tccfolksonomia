<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'post', 'namespace' => 'App\Http\Controllers'], function ($app) {
    $app->get('/', ['as' => 'post', 'uses' => 'PostController@index']);
    $app->get('create', ['as' => 'post.create', 'uses' => 'PostController@create']);
    $app->put('store', ['as' => 'post.store', 'uses' => 'PostController@store']);
    $app->get('delete/{id:[0-9]+}', ['as' => 'post.destroy', 'uses' => 'PostController@destroy']);
    $app->get('edit/{id:[0-9]+}', ['as' => 'post.edit', 'uses' => 'PostController@edit']);
    $app->patch('update/{id:[0-9]+}', ['as' => 'post.update', 'uses' => 'PostController@update']);
    $app->get('exibir/{slug}', ['as' => 'post.show', 'uses' => 'PostController@exibir']);
    $app->get('dados', ['as' => 'post.dados', 'uses' => 'PostController@dados']);
});

$app->get('/tag/listar', 'TagController@listar');
$app->get('/', 'SiteController@index');
