@extends('app')
@section('content')
<div class="container">
	    <div class="row">
	        <div class="col-md-9" role="main">
	            <div class="panel">
	            	<div class="panel-heading">
	                    <div class="text-center">
	                        <div class="row">
	                            <h3 class="pull-left">{{ $post->titulo }}</h3>
	                        </div>
	                    </div>
	                </div>
	                <div class="panel-body">
	                    {!! nl2br(e($post->post)) !!}
	                </div>
	                <div class="panel-footer">
	                    <small>Data do Post <em>{{ $post->created_at }}</em></small><br/>
	                    {!! $post->getTagsUrl() !!}
	                </div>
                </div>
	        </div>
	        <div class="col-md-3" role="complementary">
	            
	        </div>
	    </div>
	</div>
@endsection