        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://bra.ifsp.edu.br">IFSP</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('post') }}">Gerenciar Posts</a></li>
                </ul>
                <?php
                if (isset($query)):
                ?>
                <div class="col-sm-3 col-md-3">
                    {!! Form::open(array('method' => 'GET', 'url' => '/', 'class'=>'form-inline')) !!}
                        <div class="input-group input-group-sm input-group-md col-sm-12 col-md-12">
                            {!! Form::text('query', $query, ['class' => 'form-control navbar-form pull-left col-12 col-sm-8 col-lg-6', 'id'=>'query', 'placeholder'=>'Pesquisar']) !!}
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-3 col-md-3">
                    <form class="navbar-form" role="search">
                        <div class="input-group col-sm-12 col-md-12">
                            <input type="text" class="form-control" placeholder="Search" name="q">
                            <div class="input-group-btn">
                                <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                endif;
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Governo Eletrônico</a></li>
                </ul>
            </div>
        </div>