@extends('app')
@section('menu')
    @include('site._menu')
@endsection
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-9" role="main">
                <div class="row postQuantidade">
                    Utilizando o Termo (tag) "{{ $tagModel->no_termo }}" foram encontrados <b>{!! $posts->total() !!}</b> resultados.
                </div>
                <div class="panel">
                    @foreach($posts as $post)
                        <div class="panel-heading">
                            <div class="text-center">
                                <div class="row">
                                    <h3 class="pull-left text-left">{{ $post->titulo }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            {{ str_limit($post->post, 150, '...') }} <a href="{{ route('post.show', ['slug'=>$post->slug]) }}">Saiba mais</a>
                        </div>
                        <div class="panel-footer">
                            <small>Data do Post <em>{{ $post->created_at }}</em></small><br/>
                            {!! $post->getTagsUrl() !!}
                        </div>
                    @endforeach
                </div>
                <div class="row text-center postPaginacao">
                    {!! $posts->appends(Request::only('query'))->render() !!}
                </div>
                <div class="row text-center">
                    <small><em>Exibindo {!! $posts->count() !!} de {!! $posts->total() !!}</em></small>
                </div>
                <?php //echo $posts->links(); ?>
            </div>
            <div class="col-md-3" role="complementary">
                @include('post._tags')
            </div>
        </div>
    </div>
@endsection

@section('footer')
        <!-- At the bottom of your page but inside of the body tag -->
    <ol id="joyRideTipContent" data-joyride>
        <li data-class="postQuantidade" data-text="Próximo" data-options="tip_location: top; prev_button: false">
            <p>Informa a quantidade de posts disponíveis com a Tag pesquisada.</p>
        </li>
        <li data-class="postTags" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Anterior">
            <p>Você pode alterar a sua pesquisa utilizando alguma Tag.</p>
        </li>
        <li data-class="postTagsRelacionadas" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Anterior">
            <p>Alterar sua pesquisa utilizando alguma Tag Relacionada.</p>
        </li>
        <li data-class="postPaginacao"  data-button="Ok" data-prev-text="Anterior">
            <p>Ou ainda navegar através da páginas.</p>
        </li>        
    </ol>
@endsection