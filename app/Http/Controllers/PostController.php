<?php
 
namespace App\Http\Controllers;
 
use App\Post;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Pagination;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{

    private $postModel;
    public function __construct(Post $post) {
        $this->postModel = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $posts = $this->postModel->orderBy('created_at', 'desc')->paginate(10);
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $tags = \App\Tag::lists('no_termo', 'co_termo');
        return view('post.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
            'titulo' => 'required|unique:posts|max:255',
            'slug' => 'required|unique:posts|max:255',
            'post' => 'required',
            'tag_list' => 'required'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'unique'   => 'O campo :attribute já foi utilizado.',
            'max'      => 'O tamanho máximo do campo :attribute é :max caracteres.',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);       

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $post = $this->postModel->create($input);

        $tagIds = $request->input('tag_list');

        $post->tags()->attach($tagIds);

        return redirect()->route('post');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $tags = \App\Tag::lists('no_termo', 'co_termo');
        $post = $this->postModel->find($id);
        return view('post.edit')->with(compact('post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $rules = [
            'titulo' => 'required|unique:posts,id|max:255',
            'slug' => 'required|unique:posts,id|max:255',
            'post' => 'required',
            'tag_list' => 'required'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'unique'   => 'O campo :attribute já foi utilizado.',
            'max'      => 'O tamanho máximo do campo :attribute é :max caracteres.',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $post = $this->postModel->find($id)->fill($request->all());
        $post->save();
        $tagIds = $request->input('tag_list');
        $post->tags()->sync($tagIds);
        return redirect()->route('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->postModel->find($id)->delete();
        return redirect()->route('post');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return Response
     */
    public function exibir($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('post.exibir', compact('post'));
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function dados(Request $request)
    {
        $informacoes = [];
        $tags = \App\Tag::orderBy('no_termo')->get();
        foreach ($tags as $tag) {
            array_push($informacoes, (object)
                [
                    'tag'=>$tag,
                    'quantidade'=>$tag->posts->count(),
                    'diretas'=>$tag->tagsDiretas(),
                    'relacionadas'=>$tag->tagsRelacionadas()
                ]
            );
            // echo $tag->no_termo . " - " . ;
            // print_r(\App\Tag::getTermosDiretos($tag->no_termo));
        }

        $dados = (object)$informacoes;
        return view('post.dados', compact('dados'));
    }
}
 