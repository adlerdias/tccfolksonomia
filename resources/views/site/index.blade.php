@extends('app')

@section('menu')
    @include('site._menu')
@endsection

@section('content')
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1 text-center">
            <img id="firstStop" src="/img/braganca_digital.png" alt="Bragança Digital" />
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            @if($didYouMean)
                {!! $didYouMean !!}
            @endif
            @include('post._search')
        </div>
        <div class="col-sm-10 col-sm-offset-1 text-center">
            <div class="alert alert-warning" role="alert">
                <strong>Atenção!</strong> A busca só é realizada através das <em>Tags</em> oficiais do VCGE. <a id="numero1" href="/tag/listar">Você pode consultar as <b>Tags Disponíveis</b> aqui.</a>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-1 text-center">
            <small><strong>Observação:</strong> As notícias foram retiradas do <a href="http://www.brasil.gov.br/home-1/ultimas-noticias">Portal Brasil</a></small>
        </div>
    </div>
@endsection

@section('footer')
    <!-- At the bottom of your page but inside of the body tag -->
    <ol id="joyRideTipContent" data-joyride>
        <li data-id="firstStop" data-text="Próximo" data-options="tip_location: top; prev_button: false">
            <p>Olá, bem vindo ao mecanismo de pesquisa. Esse mecanismo pesquisa notícias do Portal Brasil utilizando as tags do vocabulário controlado do governo eletrônico.</p>
        </li>
        <li data-id="numero1" data-class="custom so-awesome" data-text="Próximo" data-prev-text="Anterior">
            <h4>Tags Disponíveis</h4>
            <p>Você poderá consultar as tags disponíveis para consulta, clicando aqui.</p>
        </li>
        <li data-class="numero2" data-button="Próximo" data-prev-text="Anterior" data-options="tip_location:top;tip_animation:fade">
            <h4>Pesquisa</h4>
            <p>Você pode pesquisar digitando um termo e pressionando enter, ou clicando no ícone da lupa.</p>
        </li>
        <li data-class="gerenciar" data-button="Próximo" data-prev-text="Anterior" data-options="tip_animation:fade">
            <h4>Posts</h4>
            <p>Você também pode cadastrar um novo post, utilizando o gerenciador.</p>
        </li>
        <li data-button="Ok" data-prev-text="Anterior">
            <h4>Sugestão</h4>
            <p>Caso o sistema não encontre a Tag desejada, ele fará uma sugestão utilizando as <a id="numero1" href="/tag/listar"><b>Tags Disponíveis</b></a> para a busca.</p>
        </li>
    </ol>
@endsection